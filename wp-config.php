<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'it-services' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'test1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}kbM0!vS&V^^m3]dvd(OGUv]!pW*vZ!Md74t8)yF WP7+XwlGFEpi9,,TAM+T7{e' );
define( 'SECURE_AUTH_KEY',  'JW|y/Mj^)coD}Sja,7Hwn=~Y[l=gydg-XEf5/wP[iE=CO$T!` JxG;[0Vz h>].5' );
define( 'LOGGED_IN_KEY',    'Z)vc8*@1gjXN{x7[$ R2R7JOzaLd=gSonVA^h {xCe0h-w$ii3.ApqfnIP!EJf@[' );
define( 'NONCE_KEY',        ' pSf>4~,zqg/8>#1<NCQ@{]9Y xJ=x-(e7Jawx.YopePI2dM%#2>u6Iep)r:NKqq' );
define( 'AUTH_SALT',        'Hy)>%V|0PR9F@PyyDIfcsd{|8~,aQi1EjW&DUaF07.ngSIQt5NwF0Q.<BJ,e8h;!' );
define( 'SECURE_AUTH_SALT', '/1,6rsW0W>>jC,`hr)+0p*A7#h/)>!(s@A_:xh6kh&Yi~#5;_+}jQH{iCFd2v*c#' );
define( 'LOGGED_IN_SALT',   ',B8`=>chW3nH#F=>iM_:)&_-XHiOn=dk#/jPVB/z9?B6B3z+nl8@w-JtC4*<)u-P' );
define( 'NONCE_SALT',       '}k:;y8*o#hY<DD+|J{ 76+0LyW>*kDwI(<>7aq_CoKrC4<km!:[WWv[qpAX^Mn{)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'it_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'WP_MEMORY_LIMIT', '512M' );
define('FS_METHOD', 'direct');
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
